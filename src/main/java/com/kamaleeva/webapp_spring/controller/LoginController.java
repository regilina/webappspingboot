package com.kamaleeva.webapp_spring.controller;

import com.github.scribejava.apis.FacebookApi;
import com.github.scribejava.apis.VkontakteApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.kamaleeva.webapp_spring.model.User;
import com.kamaleeva.webapp_spring.service.UserService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.concurrent.ExecutionException;


@Controller
public class LoginController {

    private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private UserService userService;

    @GetMapping(value = "/login")
    public ModelAndView login() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @PostMapping(value = "/login")
    public String login(String email, String password, HttpSession session) {

        User user = userService.findByEmail(email);
        if (user != null && password.equals(user.getPassword())) {
            session.setAttribute("userId", user.getId());
            session.setAttribute("userName", user.getName());

            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @GetMapping(value = "/logout")
    public String logout(HttpSession session) {

        session.invalidate();
        return "redirect:/login";
    }

    @GetMapping(value = "/registration")
    public ModelAndView registration() {

        ModelAndView modelAndView = new ModelAndView();
        User user = new User();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @PostMapping(value = "/registration")
    public ModelAndView registration(@Valid User user, BindingResult bindingResult) {

        ModelAndView modelAndView = new ModelAndView();
        User userExists = userService.findByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("registration");
        } else {
            userService.saveUser(user);
            modelAndView.addObject("successMessage", "User has been registered successfully");
            modelAndView.addObject("user", new User());
            modelAndView.setViewName("registration");

        }
        return modelAndView;
    }

    private static final String BASE_URL = "https://localhost:8443";
    private static final String VKONTAKTE_PROTECTED_RESOURCE_URL = "https://api.vk.com/method/users.get?v="
            + VkontakteApi.VERSION;
    private static final String VKONTAKTE_CLIENT_ID = "6792123";
    private static final String VKONTAKTE_CLIENT_SECRET = "e4SxxrlGcAqcubb5CafX";

    @RequestMapping("/vkontakte_callback")
    public String vkontakteCallback(@RequestParam String code, HttpSession session) {

        final OAuth20Service service = new ServiceBuilder(VKONTAKTE_CLIENT_ID)
                .apiSecret(VKONTAKTE_CLIENT_SECRET)
                .callback(BASE_URL + "/vkontakte_callback")
                .build(VkontakteApi.instance());

        try {
            final OAuth2AccessToken accessToken = service.getAccessToken(code);
            final OAuthRequest request = new OAuthRequest(Verb.GET, VKONTAKTE_PROTECTED_RESOURCE_URL);

            service.signRequest(accessToken, request);
            final Response response = service.execute(request);

            String vkontakteId = response.getBody().substring(19,28);
            User user = userService.findByVkontakteId(vkontakteId);
            if (user == null) {
                 user = new User();
                 user.setVkontakteId(vkontakteId);

                 String temp = response.getBody().substring(43);
                 int end = temp.indexOf(',');
                 String name = temp.substring(0, end-1);
                 user.setName(name);

                 userService.saveUser(user);
            }
            session.setAttribute("userId", user.getId());
            session.setAttribute("userName", user.getName());
            return "redirect:/home";

        } catch (InterruptedException | ExecutionException | IOException e) {
            e.printStackTrace();
            return "redirect:/login";
        }
    }

    private static final String FACEBOOK_CLIENT_ID = "332979207520572";
    private static final String FACEBOOK_CLIENT_SECRET = "a960b8eb70107c645dffa1279a0cc7c2";
    private static final String FACEBOOK_PROTECTED_RESOURCE_URL = "https://graph.facebook.com/v2.11/me";

    @RequestMapping("/facebook_callback")
    public String facebookCallback(@RequestParam String code, HttpSession session) {

        final OAuth20Service service = new ServiceBuilder(FACEBOOK_CLIENT_ID)
                .apiSecret(FACEBOOK_CLIENT_SECRET)
                .callback(BASE_URL + "/facebook_callback")
                .build(FacebookApi.instance());

        final OAuth2AccessToken accessToken;
        try {
            accessToken = service.getAccessToken(code);

            final OAuthRequest request = new OAuthRequest(Verb.GET, FACEBOOK_PROTECTED_RESOURCE_URL);
            service.signRequest(accessToken, request);
            final Response response;
            response = service.execute(request);

            String resposeBody = response.getBody();
            int start = resposeBody.indexOf(',') + 7;
            String facebookId = resposeBody.substring(start, resposeBody.length()-2);

            User user = userService.findByFacebookId(facebookId);
            if (user == null) {
                user = new User();
                user.setFacebookId(facebookId);

                String temp = response.getBody().substring(9);
                int end = temp.indexOf(',') - 1;
                String name = temp.substring(0, end);
                user.setName(name);

                userService.saveUser(user);
            }
            session.setAttribute("userId", user.getId());
            session.setAttribute("userName", user.getName());
            return "redirect:/home/";

        } catch (InterruptedException | ExecutionException | IOException e) {
            e.printStackTrace();
            return "redirect:/login";
        }
    }

}
