package com.kamaleeva.webapp_spring.controller;

import com.kamaleeva.webapp_spring.model.User;
import com.kamaleeva.webapp_spring.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class MainController {

    private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private UserService userService;

    @GetMapping(value = {"/", "/home"})
    public String home(HttpSession session) {

        Integer userId = (Integer) session.getAttribute("userId");
        if (userId == null) {
            return "redirect:/login";
        }
        return "redirect:/home/" + userId;
    }

    @GetMapping(value = "/home/{userId}")
    public ModelAndView home(@PathVariable Integer userId) {

        ModelAndView modelAndView = new ModelAndView();
        String userName = userService.findById(userId).getName();
        modelAndView.addObject("userId", userId);
        modelAndView.addObject( "userName", userName);
        modelAndView.setViewName("home");
        return modelAndView;
    }

    @GetMapping("/users")
    public ModelAndView users() {

        ModelAndView modelAndView = new ModelAndView();
        List<User> users = userService.findAll();
        modelAndView.addObject("users", users);
        modelAndView.setViewName("users");
        return modelAndView;
    }


}
