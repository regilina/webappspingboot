package com.kamaleeva.webapp_spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.kamaleeva.webapp_spring.model.Album;
import com.kamaleeva.webapp_spring.service.AlbumService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class AlbumController {

    @Autowired
    private AlbumService albumService;

    @GetMapping(value = "/albums/{userId}")
    public ModelAndView albums(@PathVariable Integer userId, HttpSession session) {

        ModelAndView modelAndView = new ModelAndView();
        List<Album> albums;

        Integer sessionUserId = (Integer)(session.getAttribute("userId"));
        if (userId.equals(sessionUserId)) {
            albums = albumService.findAllByUserId(userId);
            modelAndView.setViewName("my_albums");
        } else {
            albums = albumService.findAllByUserIdAndAccessLevel(userId, "public");
            modelAndView.setViewName("albums");
        }
        modelAndView.addObject("albums", albums);

        return modelAndView;
    }

    @PostMapping("/addAlbum")
    public String addAlbum(@RequestParam("name") String name, @RequestParam("accessLevel") String accessLevel, RedirectAttributes redirectAttributes, HttpSession session) {

        Album album = new Album();

        Integer userId = (Integer)(session.getAttribute("userId"));
        album.setUserId(userId);

        album.setName(name);
        album.setAccessLevel(accessLevel);
        albumService.saveAlbum(album);

        redirectAttributes.addFlashAttribute("message", "You successfully added album: " + name );
        return "redirect:/uploadStatus";
    }
}
