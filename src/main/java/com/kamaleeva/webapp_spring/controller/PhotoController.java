package com.kamaleeva.webapp_spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.kamaleeva.webapp_spring.model.Album;
import com.kamaleeva.webapp_spring.model.Photo;
import com.kamaleeva.webapp_spring.service.AlbumService;
import com.kamaleeva.webapp_spring.service.PhotoService;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

@Controller
public class PhotoController {

    private static final Logger LOG = LoggerFactory.getLogger(PhotoController.class);
    private static final String UPLOADED_FOLDER = "D:\\Spring\\springbootlogin\\user_photos\\";

    @Autowired
    private PhotoService photoService;

    @Autowired
    private AlbumService albumService;

    @GetMapping(value = "/photos/{albumId}")
    public ModelAndView watchPhotos(HttpSession session, @PathVariable Integer albumId) {

        List<Photo> photos = Collections.emptyList();
        Album album = albumService.findById(albumId);
        if ("public".equals(album.getAccessLevel()) || ( (album.getUserId() == (int)session.getAttribute("userId")))) {
            photos = photoService.findAllByAlbumId(albumId);
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("photos", photos);
        modelAndView.setViewName("photos");
        return modelAndView;
    }

    @PostMapping("/uploadPhoto")
    public String uploadPhoto(@RequestParam("file") MultipartFile file, @RequestParam("albumId") int albumId,
                                   RedirectAttributes redirectAttributes, HttpSession session) {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }

        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);

            Photo photo = new Photo();
            photo.setUserId((int)session.getAttribute("userId"));
            photo.setAlbumId(albumId);
            photo.setName(file.getOriginalFilename());
            photoService.savePhoto(photo);

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/uploadStatus";
    }

}
