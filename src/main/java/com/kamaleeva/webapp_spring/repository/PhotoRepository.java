package com.kamaleeva.webapp_spring.repository;

import com.kamaleeva.webapp_spring.model.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("photoRepository")
public interface PhotoRepository extends JpaRepository<Photo, Integer> {

   List<Photo> findAllByAlbumId(int albumId);
}
