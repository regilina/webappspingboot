package com.kamaleeva.webapp_spring.repository;

import com.kamaleeva.webapp_spring.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);
    User findById(int id);
    User findByVkontakteId(String id);
    User findByFacebookId(String id);
    List<User> findAll();

}