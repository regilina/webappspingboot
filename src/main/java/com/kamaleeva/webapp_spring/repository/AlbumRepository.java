package com.kamaleeva.webapp_spring.repository;

import com.kamaleeva.webapp_spring.model.Album;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("albumRepository")
public interface AlbumRepository extends CrudRepository<Album, Integer> {

   List<Album> findAllByUserId(int userId);
   List<Album> findAllByUserIdAndAccessLevel(int userId, String accessLevel);
   Album findById(int id);
}
