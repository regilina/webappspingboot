package com.kamaleeva.webapp_spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kamaleeva.webapp_spring.model.Album;
import com.kamaleeva.webapp_spring.repository.AlbumRepository;

import java.util.List;

@Service("albumService")
public class AlbumService {

    private AlbumRepository albumRepository;

    @Autowired
    public AlbumService(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    public Album findById(int id) {
        return albumRepository.findById(id);
    }

    public List<Album> findAllByUserId(int userId) {
        return albumRepository.findAllByUserId(userId);
    }

    public List<Album> findAllByUserIdAndAccessLevel(int userId, String accessLevel) {
        return albumRepository.findAllByUserIdAndAccessLevel(userId, accessLevel);
    }

    public Album saveAlbum(Album album) {
        return albumRepository.save(album);
    }

}