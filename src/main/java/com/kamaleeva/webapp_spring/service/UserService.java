package com.kamaleeva.webapp_spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kamaleeva.webapp_spring.model.Role;
import com.kamaleeva.webapp_spring.model.User;
import com.kamaleeva.webapp_spring.repository.RoleRepository;
import com.kamaleeva.webapp_spring.repository.UserRepository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

@Service("userService")
public class UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    public User findById(int id) {
        return userRepository.findById(id);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User findByVkontakteId(String vkontakteId) {
        return userRepository.findByVkontakteId(vkontakteId);
    }

    public User findByFacebookId(String facebookId) {
        return userRepository.findByFacebookId(facebookId);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User saveUser(User user) {

        if (null == user.getEmail()) {
            user.setEmail("temp@temp");
            String password = String.valueOf(new Random().nextInt());
            user.setPassword(password);
            user.setLastName("Unknown");
        }

        user.setActive(1);
        Role userRole = roleRepository.findByRole("USER");
        user.setRoles(new HashSet<>(Arrays.asList(userRole)));
        return userRepository.save(user);
    }
}
