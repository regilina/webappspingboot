package com.kamaleeva.webapp_spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kamaleeva.webapp_spring.model.Photo;
import com.kamaleeva.webapp_spring.repository.PhotoRepository;

import java.util.List;

@Service("photoService")
public class PhotoService {

    private PhotoRepository photoRepository;

    @Autowired
    public PhotoService(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    public Photo savePhoto(Photo photo) {
        return photoRepository.save(photo);

    }

    public List<Photo> findAllByAlbumId(Integer albumId) {
        return photoRepository.findAllByAlbumId(albumId);
    }
}